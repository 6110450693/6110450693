package bookticket;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MovieController {
    private String Username;
    @FXML
    public void StarWars(MouseEvent event) throws Exception{
        ImageView a = (ImageView) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("theater.fxml"));
        stage.setScene(new Scene(loader.load(),600, 800));
        stage.setTitle("StarWars");
        TheaterController theaterController = loader.getController();
        theaterController.setUsername(Username);
        stage.show();
    }
    @FXML
    public void It(MouseEvent event) throws Exception{
        ImageView a = (ImageView) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("theater2.fxml"));
        stage.setScene(new Scene(loader.load(),600, 800));
        stage.setTitle("It 2");
        TheaterController theaterController = loader.getController();
        theaterController.setUsername(Username);
        stage.show();
    }
    @FXML
    public void SpiderMan(MouseEvent event) throws Exception{
        ImageView a = (ImageView) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("theater3.fxml"));
        stage.setScene(new Scene(loader.load(),600, 800));
        stage.setTitle("Spider Man :Far for home");
        TheaterController theaterController = loader.getController();
        theaterController.setUsername(Username);
        stage.show();
    }


    @FXML
    public void Logout(ActionEvent event) throws Exception{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
        stage.setScene(new Scene(loader.load(),600, 400));
        stage.setTitle("LoginToCinemaBookTicket");
        stage.show();
    }
    public void setUsername(String username){
        Username = username;
    }
}
