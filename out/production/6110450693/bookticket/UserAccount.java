package bookticket;

import java.util.ArrayList;

public class UserAccount {
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
    private ArrayList<UserAccount> user = new ArrayList<UserAccount>();

    public UserAccount(String firstName, String lastName, String email, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
    }


    public UserAccount(){}

    public void add(UserAccount userAccount){
        user.add(userAccount);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public ArrayList<UserAccount> getUser() {
        return user;
    }
}
