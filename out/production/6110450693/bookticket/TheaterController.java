package bookticket;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TheaterController {
    private String Username;

    public void setUsername(String username){
        Username = username;
    }
    public void initialize() throws IOException {
        File file = new File("BookingData.csv");
        if(file.exists()){}
        else{
            file.createNewFile();
        }
    }
    @FXML
    public void GoToBookTheater1(ActionEvent event) throws Exception{
        Button a = (Button) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("room1.fxml"));
        stage.setScene(new Scene(loader.load(),600, 800));
        stage.setTitle("Theater 1 : ระบบฉายธรรมดาและที่นั่งธรรมดา");
        TheaterRoom1 theaterRoom1 = loader.getController();
        theaterRoom1.setUsername(Username);
        BookSeatNormal bookSeatNormal = loader.getController();
        bookSeatNormal.setRoom("Room1");
        stage.show();
    }
    @FXML
    public void GoToBookTheater2(ActionEvent event) throws Exception{
        Button a = (Button) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("room2.fxml"));
        stage.setScene(new Scene(loader.load(),600, 800));
        stage.setTitle("Theater 2 : ระบบฉาย 4K และที่นั่งธรรมดา");
        TheaterRoom2 theaterRoom2 = loader.getController();
        theaterRoom2.setUsername(Username);
        BookSeatNormal bookSeatNormal = loader.getController();
        bookSeatNormal.setRoom("Room2");
        stage.show();
    }
    @FXML
    public void GoToBookTheater3(ActionEvent event) throws Exception{
        Button a = (Button) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("room3.fxml"));
        stage.setScene(new Scene(loader.load(),600, 800));
        stage.setTitle("Theater 3 : ระบบฉาย 4K และที่นั่งแบบผสม");
        TheaterRoom3 theaterRoom3 = loader.getController();
        theaterRoom3.setUsername(Username);
        BookSeatMix bookSeatMix = loader.getController();
        bookSeatMix.setRoom("Room3");
        stage.show();
    }
    @FXML
    public void GoToBookTheater4(ActionEvent event) throws Exception{
        Button a = (Button) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("room4.fxml"));
        stage.setScene(new Scene(loader.load(),600, 800));
        stage.setTitle("Theater 4 : ระบบฉาย 3D และที่นั่งแบบผสม");
        TheaterRoom4 theaterRoom4 = loader.getController();
        theaterRoom4.setUsername(Username);
        BookSeatMix bookSeatMix = loader.getController();
        bookSeatMix.setRoom("Room4");
        stage.show();
    }
    @FXML
    public void GoToBookTheater5(ActionEvent event) throws Exception{
        Button a = (Button) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("room5.fxml"));
        stage.setScene(new Scene(loader.load(),600, 800));
        stage.setTitle("Theater 5 : ระบบฉาย 4DX และที่นั่งธรรมดา");
        TheaterRoom5 theaterRoom5 = loader.getController();
        theaterRoom5.setUsername(Username);
        BookSeatNormal bookSeatNormal = loader.getController();
        bookSeatNormal.setRoom("Room5");
        stage.show();
    }
    @FXML
    public void GoToBack(ActionEvent event) throws Exception{
        Button a = (Button) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("movie.fxml"));
        stage.setScene(new Scene(loader.load(),600, 800));
        stage.setTitle("Show time");
        MovieController movieController = loader.getController();
        movieController.setUsername(Username);
        stage.show();
    }


}
