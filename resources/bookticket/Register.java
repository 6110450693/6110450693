package bookticket;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import java.io.*;

public class Register {
    @FXML
    TextField firstName,lastName,email,username,password;
    @FXML
    Label status;
    UserAccount userAccount = new UserAccount();
    public void initialize() throws IOException {
        File file = new File("User.csv");
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = null;
        while((line = bufferedReader.readLine()) != null){
            String[] user = line.split(",");
            userAccount.add(new UserAccount(user[0],user[1],user[2],user[3],user[4]));
        }
    }
    @FXML
    public void CreateUser(ActionEvent event) throws IOException {
        userAccount.add(new UserAccount(firstName.getText(),lastName.getText(),email.getText(),username.getText(),password.getText()));
        writer();
    }
    public void writer() throws IOException {
        File file = new File("User.csv");
        FileReader fileReader = new FileReader(file);
        FileWriter fileWriter = new FileWriter(file,true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = null;
        bufferedWriter.newLine();
        while((line = bufferedReader.readLine()) != null){
            String[] user = line.split(",");
            userAccount.add(new UserAccount(user[0],user[1],user[2],user[3],user[4]));
        }
        for (UserAccount u : userAccount.getUser()){
            if (username.getText().equals(u.getUsername())){
                status.setText("Username is Duplicate");
                break;
            }
            else{
                bufferedWriter.write(firstName.getText() + ",");
                bufferedWriter.write(lastName.getText() + ",");
                bufferedWriter.write(email.getText() + ",");
                bufferedWriter.write(username.getText() + ",");
                bufferedWriter.write(password.getText() + "");
                status.setText("Register complete");
            }
            bufferedWriter.close();
        }
    }
    @FXML
    public void GoToBack(ActionEvent event) throws Exception{
        Button a = (Button) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
        stage.setScene(new Scene(loader.load(),600, 400));
        stage.setTitle("LoginToCinemaBookTicket");
        stage.show();
    }
}
