package bookticket;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;

public class LoginController {
    @FXML
    TextField username,password;
    @FXML
    Label status;
    UserAccount userAccount = new UserAccount();
    public void initialize() throws IOException {
        File file = new File("User.csv");
        if(file.exists()){
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = null;
            while((line = bufferedReader.readLine()) != null){
                String[] user = line.split(",");
                userAccount.add(new UserAccount(user[0],user[1],user[2],user[3],user[4]));
            }
        }
        else{
            file.createNewFile();
            FileWriter fileWriter1 = new FileWriter(file,true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter1);
            bufferedWriter.write("java"+ ",");
            bufferedWriter.write("work" + ",");
            bufferedWriter.write("javaWork" + ",");
            bufferedWriter.write("cs12345" + ",");
            bufferedWriter.write("12345" + "");
            bufferedWriter.newLine();
            bufferedWriter.write("jason"+ ",");
            bufferedWriter.write("useful" + ",");
            bufferedWriter.write("jasonUseful" + ",");
            bufferedWriter.write("up54321" + ",");
            bufferedWriter.write("54321" + "");
            bufferedWriter.close();
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = null;
            while((line = bufferedReader.readLine()) != null){
                String[] user2 = line.split(",");
                userAccount.add(new UserAccount(user2[0],user2[1],user2[2],user2[3],user2[4]));
            }
        }


    }
    @FXML
    public void Login(ActionEvent event) throws IOException {
        status.setText("");
        for (UserAccount u : userAccount.getUser()){
            if (username.getText().equals(u.getUsername()) && (password.getText().equals(u.getPassword()))){
                Button a = (Button) event.getSource();
                Stage stage = (Stage) a.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("movie.fxml"));
                stage.setScene(new Scene(loader.load(),600, 800));
                stage.setTitle("Movie");
                MovieController movieController = loader.getController();
                movieController.setUsername(username.getText());
                stage.show();
            }
            else {
                if (username.getText().equals(u.getUsername()) && (!password.getText().equals(u.getUsername()))){
                    status.setText("Password incorrect");
                    break;
                }
                else if ((!username.getText().equals(u.getUsername()))&&(password.getText().equals(u.getPassword()))||(password.getText().equalsIgnoreCase(""))){
                    status.setText("Username not found");
                    break;
                }
                else {
                    status.setText("Password incorrect");
                }
            }
        }
    }
    @FXML
    public void handleGoToRegister(ActionEvent event) throws Exception{
        Button a = (Button) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("register.fxml"));
        stage.setScene(new Scene(loader.load(),600, 400));
        stage.setTitle("Register");
        stage.show();
    }
    @FXML
    public void GoProfile(ActionEvent event) throws Exception{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Creator.fxml"));
        stage.setScene(new Scene(loader.load(),600, 800));
        stage.setTitle("Show time");
        stage.show();
    }

}
