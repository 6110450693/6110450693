package bookticket;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;

public class TheaterRoom5 extends BookSeatNormal{
    @FXML
    GridPane seat;
    private String Username;
    private ArrayList<String> clickSeat = new ArrayList<>();
    private String Room;

    public void setClickSeat(ArrayList<String>clickSeat){
        super.clickSeat = clickSeat;
    }
    public void setUsername(String username){
        Username = username;
    }
    public void setRoom(String room){Room = room;}
    public void initialize() throws IOException {
        setSeat();
        setClickSeat(clickSeat);
    }
    @FXML
    public void GoToBack(ActionEvent event) throws Exception{
        Button a = (Button) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("theater3.fxml"));
        stage.setScene(new Scene(loader.load(),600, 800));
        stage.setTitle("Spider man");
        TheaterController theaterController = loader.getController();
        theaterController.setUsername(Username);
        stage.show();
    }
    public void GoBuySeat(ActionEvent event) throws Exception{
        Button a = (Button) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("buyseat.fxml"));
        stage.setScene(new Scene(loader.load(),600, 400));
        stage.setTitle("Confirm?");
        BuySeat buySeat = loader.getController();
        buySeat.setRoom(Room);
        buySeat.setUsername(Username);
        buySeat.setClickSeat(clickSeat);
        buySeat.setShowBooking();
        stage.show();
    }
    public void checkSeat(String bookSeat,ImageView imageView) throws IOException {
        File file = new File("BookingData.csv");
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            String[] a = line.split(",");
            if (a[1].equals("Room5")) {
                String[] b = a[2].split("/");
                for (String c : b) {
                    if (bookSeat.equals(c)) {
                        imageView.setImage(new Image("image/Chair3.png"));
                        imageView.setDisable(true);
                    }
                }
            }
        }
    }
}
