package bookticket;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;

public class BookSeatMix {
    @FXML
    GridPane seat1,seat2;
    ArrayList<String> clickSeat = new ArrayList<>();
    AtomicReference<String> check = new AtomicReference<>("0");
    String Room;
    public void setRoom(String room){Room = room;}
    public void setSeatMark(ImageView imageView,int i,int j) throws IOException {
        String str = "";
        if (i == 0){
            str = ("A" +(j+1));
        }
        else if(i == 1){
            str = ("B" +(j+1));
        }
        else if(i == 2){
            str = ("C" +(j+1));
        }
        else if (i == 3){
            str = ("D"+(j+1));
        }
        checkSeat(str,imageView);
    }
    public void setSeatMarkPre(ImageView imageView,int i) throws IOException {
        String pre = "";
        if (i == 0){
            pre = ("P" +(i+1));
        }
        else if (i == 1){
            pre = ("P" +(i+1));
        }
        else if (i == 2){
            pre = ("P" +(i+1));
        }
        else if (i == 3){
            pre = ("P" +(i+1));
        }
        checkSeat(pre,imageView);
    }
    public void checkSeat(String bookSeat,ImageView imageView) throws IOException {
        File file = new File("BookingData.csv");
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = null;
        while ((line = bufferedReader.readLine()) != null){
            String[] a = line.split(",");
            if (a[1].equals("Room3")){
                String[] b = a[2].split("/");
                for (String c : b){
                    if (bookSeat.equals(c)){
                        imageView.setImage(new Image("image/Chair3.png"));
                        imageView.setDisable(true);
                    }
                }
            }
            else if (a[1].equals("Room4")){
                String[] b = a[2].split("/");
                for (String c : b){
                    if (bookSeat.equals(c)){
                        imageView.setImage(new Image("image/Chair3.png"));
                        imageView.setDisable(true);
                    }
                }
            }
        }
    }

    public void clickSeat(ImageView imageView, String check,int i,int j){
        if (check.equals("0") && (i == 0)){
            clickSeat.add("A"+(j+1));
            imageView.setImage(new Image("image/select.png"));
            System.out.println(clickSeat);
        }
        else if (check.equals("0") && (i == 1)){
            clickSeat.add("B"+(j+1));
            imageView.setImage(new Image("image/select.png"));
            System.out.println(clickSeat);
        }
        else if (check.equals("0") && (i == 2)){
            clickSeat.add("C"+(j+1));
            imageView.setImage(new Image("image/select.png"));
            System.out.println(clickSeat);
        }
        else if (check.equals("0") && (i == 3)){
            clickSeat.add("D"+(j+1));
            imageView.setImage(new Image("image/select.png"));
            System.out.println(clickSeat);
        }
        if(check.equals("1") && (i == 0)){
            clickSeat.remove("A"+(j+1));
            imageView.setImage(new Image("image/Chair1.png"));
            System.out.println(clickSeat);
        }
        else if(check.equals("1") && (i == 1)){
            clickSeat.remove("B"+(j+1));
            imageView.setImage(new Image("image/Chair1.png"));
            System.out.println(clickSeat);
        }
        else if(check.equals("1") && (i == 2)){
            clickSeat.remove("C"+(j+1));
            imageView.setImage(new Image("image/Chair1.png"));
            System.out.println(clickSeat);
        }
        else if(check.equals("1") && (i == 3)){
            clickSeat.remove("D"+(j+1));
            imageView.setImage(new Image("image/Chair1.png"));
            System.out.println(clickSeat);
        }
    }
    public void clickSeatPre(ImageView imageView, String check,int i){
        if (check.equals("0") && (i == 0)){
            clickSeat.add("P"+(i+1));
            imageView.setImage(new Image("image/select.png"));
            System.out.println(clickSeat);
        }
        else if (check.equals("0") && (i == 1)){
            clickSeat.add("P"+(i+1));
            imageView.setImage(new Image("image/select.png"));
            System.out.println(clickSeat);
        }
        else if (check.equals("0") && (i == 2)){
            clickSeat.add("P"+(i+1));
            imageView.setImage(new Image("image/select.png"));
            System.out.println(clickSeat);
        }
        else if (check.equals("0") && (i == 3)){
            clickSeat.add("P"+(i+1));
            imageView.setImage(new Image("image/select.png"));
            System.out.println(clickSeat);
        }
        if(check.equals("1") && (i == 0)){
            clickSeat.remove("P"+(i+1));
            imageView.setImage(new Image("image/Chair2.png"));
            System.out.println(clickSeat);
        }
        else if(check.equals("1") && (i == 1)){
            clickSeat.remove("P"+(i+1));
            imageView.setImage(new Image("image/Chair2.png"));
            System.out.println(clickSeat);
        }
        else if(check.equals("1") && (i == 2)){
            clickSeat.remove("P"+(i+1));
            imageView.setImage(new Image("image/Chair2.png"));
            System.out.println(clickSeat);
        }
        else if(check.equals("1") && (i == 3)){
            clickSeat.remove("P"+(i+1));
            imageView.setImage(new Image("image/Chair2.png"));
            System.out.println(clickSeat);
        }
    }
    public void setSeat() throws IOException {
        ImageView normalSeat = new ImageView();
        ImageView premiumSeat = new ImageView();
        for (int i = 0;i < 4;i++){
            for (int j = 0;j < 4;j++){
                normalSeat = new ImageView(new Image("image/Chair1.png"));
                premiumSeat = new ImageView(new Image("image/Chair2.png"));
                if (j == 0 && i < 4){
                    premiumSeat.setFitWidth(50);
                    premiumSeat.setFitHeight(50);
                    premiumSeat.setPickOnBounds(true);
                    premiumSeat.setPreserveRatio(true);
                    ImageView finalPremiumSeat = premiumSeat;
                    int finalI1 = i;
                    setSeatMarkPre(premiumSeat,i);
                    premiumSeat.setOnMouseClicked(event -> {
                        clickSeatPre(finalPremiumSeat,check.get(), finalI1);
                        for (int a = 0;a < 9999;a++){
                            if (check.get().equals("0")){
                                check.set("1");
                            }
                            else {
                                check.set("0");
                            }
                        }
                    });
                    seat2.add(premiumSeat,i,j);
                }
                normalSeat.setFitWidth(50);
                normalSeat.setFitHeight(50);
                normalSeat.setPreserveRatio(true);
                normalSeat.setPickOnBounds(true);
                ImageView finalNormalSeat = normalSeat;
                int finalI = i;
                int finalJ = j;
                setSeatMark(normalSeat,i,j);
                normalSeat.setOnMouseClicked(event -> {
                    clickSeat(finalNormalSeat, check.get(), finalI, finalJ);

                    for (int a = 0;a < 9999;a++){
                        if (check.get().equals("0")){
                            check.set("1");
                        }
                        else {
                            check.set("0");
                        }
                    }
                });
                seat1.add(normalSeat, i, j);

            }
        }


    }

}
