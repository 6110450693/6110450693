package bookticket;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;

public class BookSeatNormal {
    @FXML
    GridPane seat;
    private String Room;
    public void setRoom(String room){Room = room;}
    ArrayList<String> clickSeat = new ArrayList<>();
    AtomicReference<String> check = new AtomicReference<>("0");
    public void setSeatMark(ImageView imageView,int i,int j) throws IOException {
        String str = "";
        if (i == 0){
            str = ("A" +(j+1));
        }
        else if(i == 1){
            str = ("B" +(j+1));
        }
        else if(i == 2){
            str = ("C" +(j+1));
        }
        else if (i == 3){
            str = ("D"+(j+1));
        }
        checkSeat(str,imageView);
    }

    public void checkSeat(String bookSeat,ImageView imageView) throws IOException {
        File file = new File("BookingData.csv");
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = null;
        while ((line = bufferedReader.readLine()) != null){
            String[] a = line.split(",");
            if (a[1].equals(Room)){
                String[] b = a[2].split("/");
                for (String c : b){
                    if (bookSeat.equals(c)){
                        imageView.setImage(new Image("image/chair3.png"));
                        imageView.setDisable(true);
                    }
                }
            }
        }
    }

    public void clickSeat(ImageView imageView, String check,int i,int j){
        if (check.equals("0") && (i == 0)){
            clickSeat.add("A"+(j+1));
            imageView.setImage(new Image("image/select.png"));
            System.out.println(clickSeat);
        }
        else if (check.equals("0") && (i == 1)){
            clickSeat.add("B"+(j+1));
            imageView.setImage(new Image("image/select.png"));
            System.out.println(clickSeat);
        }
        else if (check.equals("0") && (i == 2)){
            clickSeat.add("C"+(j+1));
            imageView.setImage(new Image("image/select.png"));
            System.out.println(clickSeat);
        }
        else if (check.equals("0") && (i == 3)){
            clickSeat.add("D"+(j+1));
            imageView.setImage(new Image("image/select.png"));
            System.out.println(clickSeat);
        }
        if(check.equals("1") && (i == 0)){
            clickSeat.remove("A"+(j+1));
            imageView.setImage(new Image("image/Chair1.png"));
            System.out.println(clickSeat);
        }
        else if(check.equals("1") && (i == 1)){
            clickSeat.remove("B"+(j+1));
            imageView.setImage(new Image("image/Chair1.png"));
            System.out.println(clickSeat);
        }
        else if(check.equals("1") && (i == 2)){
            clickSeat.remove("C"+(j+1));
            imageView.setImage(new Image("image/Chair1.png"));
            System.out.println(clickSeat);
        }
        else if(check.equals("1") && (i == 3)){
            clickSeat.remove("D"+(j+1));
            imageView.setImage(new Image("image/Chair1.png"));
            System.out.println(clickSeat);
        }
    }
    public void setSeat() throws IOException {
        ImageView normalSeat = new ImageView();
        for (int i = 0;i < 4;i++){
            for (int j = 0;j < 4;j++){
                normalSeat = new ImageView(new Image("image/Chair1.png"));
                normalSeat.setFitWidth(50);
                normalSeat.setFitHeight(50);
                normalSeat.setPreserveRatio(true);
                normalSeat.setPickOnBounds(true);
                ImageView finalNormalSeat = normalSeat;
                int finalI = i;
                int finalJ = j;
                setSeatMark(normalSeat,i,j);
                normalSeat.setOnMouseClicked(event -> {
                    clickSeat(finalNormalSeat, check.get(), finalI, finalJ);
                    for (int a = 0;a < 9999;a++){
                        if (check.get().equals("0")){
                            check.set("1");
                        }
                        else {
                            check.set("0");
                        }
                    }
                });
                seat.add(normalSeat, i, j);
            }
        }

    }
}

