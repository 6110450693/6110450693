package bookticket;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.Channel;
import java.util.ArrayList;

public class BuySeat {
    @FXML
    Label showBooking;
    @FXML
    Button buy;
    private String Username;
    private String Room;
    private ArrayList<String> clickSeat;
    private String seat = "";
    private int price;
    private int pricePre;

    public void setClickSeat(ArrayList<String> clickSeat) {
        this.clickSeat = clickSeat;
    }

    public void setRoom(String room) {
        Room = room;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public void setShowBooking() {
        for (String a : clickSeat) {
            seat += a + "/";
            if ((a.equals("P1")) || (a.equals("P2"))) {
                pricePre += 1;
            } else if ((a.equals("P3")) || (a.equals("P4"))) {
                pricePre += 1;
            } else {
                price += 1;
            }
        }
        if (Room.equals("Room1")) {
            price = price * 100;
            showBooking.setText("Username = " + Username + ",TheaterRoom = " + Room + ",Book at = " + seat + " Total price = " + price);
        } else if (Room.equals("Room2")) {
            price = price * 190;
            showBooking.setText("Username = " + Username + ",TheaterRoom = " + Room + ",Book at = " + seat + " Total price = " + price);
        } else if (Room.equals("Room3")) {
            pricePre = pricePre * 300;
            price = price * 200 + pricePre;

            showBooking.setText("Username = " + Username + ",TheaterRoom = " + Room + ",Book at = " + seat + " Total price = " + price);
        } else if (Room.equals("Room4")) {
            pricePre = pricePre * 400;
            price = price * 300 + pricePre;

            showBooking.setText("Username = " + Username + ",TheaterRoom = " + Room + ",Book at = " + seat + " Total price = " + price);
        } else if (Room.equals("Room5")) {
            price = price * 190;
            showBooking.setText("Username = " + Username + ",TheaterRoom = " + Room + ",Book at = " + seat + " Total price = " + price);
        }
    }

    @FXML
    public void buySeat(ActionEvent event) throws IOException {
        File file = new File("BookingData.csv");
        FileWriter fileWriter = new FileWriter(file, true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write("" + Username + "," + Room + "," + seat);
        bufferedWriter.newLine();
        bufferedWriter.close();
        showBooking.setText("Booking Done!");
        buy.setDisable(true);
}
    @FXML
    public void Cancel(ActionEvent event) throws IOException {
        Button a = (Button) event.getSource();
        Stage stage = (Stage) a.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("movie.fxml"));
        stage.setScene(new Scene(loader.load(),600, 800));
        stage.setTitle("Movie");
        MovieController movieController = loader.getController();
        movieController.setUsername(Username);
        stage.show();
    }

}
